
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Statement;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.crypto.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import sun.misc.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Chintan
 */

class MouseListener extends MouseAdapter{
    MainFrame mf;
    JLabel previousReceiver=null;
    JLabel currentReceiver=null;
    MouseListener(MainFrame obj){
        mf=obj;
    }
    public void mouseClicked(MouseEvent me){
        String name;
        
        if(previousReceiver==null){
            currentReceiver=(JLabel)me.getSource();
            previousReceiver=currentReceiver;
        }else{
            previousReceiver=currentReceiver;
            currentReceiver=(JLabel)me.getSource();
            previousReceiver.setForeground(new Color(115,143,180));
        }
        currentReceiver.setForeground(new Color(255,255,255));
        
        name = currentReceiver.getText();
        System.out.println("this receiver "+name);
        mf.setReceiverName(name);
        mf.getMsgPane().removeAll();
        System.out.println("");
        mf.XMLReader();
    }
}
public class MainFrame extends javax.swing.JFrame{
    Socket ClientSocket;
    Connection conn;
    Statement statement=null;
    ResultSet rs=null;
    PreparedStatement ps=null;
    String clientName;
    String CurrentReceiver="";
    PrintWriter output;
    int length=1;
    File inFile;
    File outFile;
    GridLayout gl;
    final String right="RIGHT";
    final String left="LEFT";
    int rowCounter,row=0;
    void setReceiverName(String Receiver){
        this.CurrentReceiver=Receiver;
        ThisReceiiver.setText(Receiver);
        gl.setRows(20);
    }
    void addMsg(String str,String position,String receiver){
        try{
            if(!("".equals(CurrentReceiver))){
                if(CurrentReceiver.equals(receiver)){
                    JLabel lbl=new JLabel("");
                    if(rowCounter>=row){
                        gl.setRows(gl.getRows()+1);
                    }
                    lbl.setPreferredSize(new Dimension(595,30));
                    if("LEFT".equals(position)){
                        lbl=new JLabel(str,JLabel.LEFT);
                    }else if("RIGHT".equals(position)){
                        lbl=new JLabel(str,JLabel.RIGHT);
                    }
                    rowCounter++;
                    lbl.setForeground(new Color(255,255,255));
                    msgpane1.add(lbl);
                    msgpane1.updateUI();
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }
    JPanel getMsgPane(){
        return msgpane1;
    }
    void addUsersFromDB(){
        try {
            UserList.removeAll();
            statement=conn.createStatement();
            String sql="select username from user";
            ps=conn.prepareStatement(sql);
            rs=ps.executeQuery();
            int y=1;
            JLabel receiver=null;
            String res;
            GridLayout g1=new GridLayout(10,10);
            UserList.setLayout(g1);
            MouseListener ml=new MouseListener(this);
            while(rs.next()){
                res=rs.getString("username");
                
                if(!clientName.equals(res)){
                    g1.setRows(y);
                    receiver=new JLabel(rs.getString("username"));
                    receiver.setPreferredSize(new Dimension(290,30));
                    receiver.setForeground(new Color(115,143,180));
                    UserList.add(receiver);
                    receiver.addMouseListener(ml);
                    y++;
                }
            }
            UserList.updateUI();
        } catch (SQLException ex) {
            System.out.println("exception while fetching users");
        }catch (Exception ex) {
            System.out.println(ex);
        }
    }
    public MainFrame(String name) {
        initComponents();
        try{
            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/userlist","chintan2","chintan");;
            System.out.println(name);
            CurrentUserlbl.setText(name);
            clientName=name;
            gl=new GridLayout(20,20);
            msgpane1.setLayout(gl);
            
            Socket ClientSocket=new Socket("localhost",5000);
            // socket.setSoTimeout(5000);
            BufferedReader input=new BufferedReader(new InputStreamReader(ClientSocket.getInputStream()));
            output=new PrintWriter(ClientSocket.getOutputStream(),true);
            output.println(clientName);
            addUsersFromDB();
            new Echoer(ClientSocket,this).start();
        }catch(SocketTimeoutException e){
//            System.out.println("Issue : "+e.getMessage());
        }catch(IOException e){
//            System.out.println("Issue : "+e.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        ClosePane = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        CurrentUserlbl = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        txtmsg = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        msgpane1 = new javax.swing.JPanel();
        ReceiverPanel = new javax.swing.JPanel();
        ThisReceiiver = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        UserList = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(198, 212, 238));

        jPanel2.setBackground(new java.awt.Color(6, 26, 64));
        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        ClosePane.setBackground(new java.awt.Color(6, 26, 64));
        ClosePane.setForeground(new java.awt.Color(198, 212, 238));

        jLabel1.setBackground(new java.awt.Color(6, 26, 64));
        jLabel1.setFont(new java.awt.Font("Roboto", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(198, 212, 238));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("X");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout ClosePaneLayout = new javax.swing.GroupLayout(ClosePane);
        ClosePane.setLayout(ClosePaneLayout);
        ClosePaneLayout.setHorizontalGroup(
            ClosePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );
        ClosePaneLayout.setVerticalGroup(
            ClosePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ClosePaneLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel3.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(198, 212, 238));
        jLabel3.setText("Current Client:-");

        CurrentUserlbl.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        CurrentUserlbl.setForeground(new java.awt.Color(198, 212, 238));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(352, 352, 352)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CurrentUserlbl, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ClosePane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ClosePane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(CurrentUserlbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(12, 9, 13));
        jPanel4.setForeground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(105, 105, 105));

        txtmsg.setBackground(new java.awt.Color(169, 169, 169));
        txtmsg.setForeground(new java.awt.Color(12, 9, 13));
        txtmsg.setBorder(null);

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("SEND");
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(txtmsg, javax.swing.GroupLayout.PREFERRED_SIZE, 545, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(txtmsg, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jScrollPane1.setBorder(null);
        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        msgpane1.setBackground(new java.awt.Color(12, 9, 13));
        msgpane1.setForeground(new java.awt.Color(255, 255, 255));
        msgpane1.setLayout(new java.awt.GridLayout(1, 0));
        jScrollPane1.setViewportView(msgpane1);

        ReceiverPanel.setBackground(new java.awt.Color(0, 39, 72));
        ReceiverPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        ReceiverPanel.setForeground(new java.awt.Color(0, 39, 72));

        ThisReceiiver.setBackground(new java.awt.Color(255, 255, 255));
        ThisReceiiver.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        ThisReceiiver.setForeground(new java.awt.Color(198, 212, 238));

        javax.swing.GroupLayout ReceiverPanelLayout = new javax.swing.GroupLayout(ReceiverPanel);
        ReceiverPanel.setLayout(ReceiverPanelLayout);
        ReceiverPanelLayout.setHorizontalGroup(
            ReceiverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ReceiverPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ThisReceiiver, javax.swing.GroupLayout.PREFERRED_SIZE, 574, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        ReceiverPanelLayout.setVerticalGroup(
            ReceiverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ReceiverPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(ThisReceiiver, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addComponent(ReceiverPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(ReceiverPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        jPanel3.setBackground(new java.awt.Color(6, 26, 64));

        jScrollPane3.setBorder(null);

        UserList.setBackground(new java.awt.Color(6, 26, 64));
        UserList.setForeground(new java.awt.Color(198, 212, 238));
        UserList.setLayout(new java.awt.GridLayout(1, 0));
        jScrollPane3.setViewportView(UserList);

        jPanel6.setBackground(new java.awt.Color(6, 26, 64));
        jPanel6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(198, 212, 238));
        jLabel4.setText("Refresh");
        jLabel4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(259, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(19, 19, 19))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private String readFile(String receiver){
        char c;
        int b;
        String UserList="";
        FileInputStream in=null;
        try{
//            System.out.println("Reading File "+clientName+CurrentReceiver);
            inFile=new File(clientName+receiver+".xml");
            in=new FileInputStream(inFile);
            while((b=in.read())!=-1){
                c=(char)b;
                UserList+=c;
            }
        }catch(Exception e){}
        return UserList;
    }
    void XMLWriter(String msg,String receiver){
        FileOutputStream out=null;
        try{
            outFile=new File(clientName+receiver+".xml");
            String UserList=readFile(receiver);
            System.out.println("Writing:- "+UserList);
            String str;
            str = "<"+receiver+">";
            str+="<msg>"+msg+"</msg>";
            str+="</"+receiver+">";
            
            UserList+=str;
            out=new FileOutputStream(outFile);
            for(int i=0;i<UserList.length();i++){
                out.write(UserList.charAt(i));
            }
        }catch(Exception e){}
    }
    void XMLReader(){
        Pattern h2TextGroupPattern;
        Matcher h2TextGroupMatcher;
        try{
            msgpane1.removeAll();
            String UserList=readFile(CurrentReceiver);
            System.out.println("Reading:- "+UserList);
            String str;
            length=10;
            int i=1;
            Pattern pattern;
            Matcher matcher;
            String arr[];
            str="(<"+CurrentReceiver+">)(<msg>)(.*?)(</msg>)(</"+CurrentReceiver+">)";
            pattern = Pattern.compile(str);
            matcher=pattern.matcher(UserList);
            while(matcher.find()){
//                System.out.println("\n\n===="+matcher.group(3));
//                System.out.println("Executing matcher for "+(i++));
                
                System.out.println(matcher.group(3));
                String htmlText=(matcher.group(3));
                    
                String h2TextGroupRegex2=":-.?";
                h2TextGroupPattern=Pattern.compile(h2TextGroupRegex2);
                h2TextGroupMatcher=h2TextGroupPattern.matcher(htmlText);
                JLabel lbl;
//                System.out.println("matcher inside matcher "+h2TextGroupMatcher.find());
                h2TextGroupMatcher.reset();
                if(h2TextGroupMatcher.find()){
//                    System.out.println("1");
                    arr=htmlText.split(":-");
                    addMsg(arr[1],right,CurrentReceiver);                        
                }else{
                    addMsg(matcher.group(3),left,CurrentReceiver);
                }
//                System.out.println("3");
                msgpane1.updateUI();
            }
//            System.out.println("updating UI");
            msgpane1.updateUI();
        }catch(Exception e){}
    }
    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        //this method will send msg to the server with Nae of Current class ,message and Receiver's name 
        String str;
	String res;
//        System.out.println("CurrentReceiver :"+CurrentReceiver);
        res=CurrentReceiver;
        str=txtmsg.getText();
        output.println(res+":-:"+str+":-:"+clientName);
        addMsg(str,right,CurrentReceiver);
        XMLWriter(":-"+txtmsg.getText(),CurrentReceiver);
        txtmsg.setText("");
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        System.exit(0);
        ClosePane.setBackground(new Color(255,255,255));
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        // TODO add your handling code here:
        addUsersFromDB();
    }//GEN-LAST:event_jLabel4MouseClicked

    private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
        // TODO add your handling code here:
        ClosePane.setBackground(new Color(198,212,238));
        ClosePane.setForeground(new Color(0,39,72));
    }//GEN-LAST:event_jLabel1MouseEntered

    private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
        // TODO add your handling code here:
        ClosePane.setBackground(new Color(0,39,72));
        ClosePane.setForeground(new Color(198,212,238));
    }//GEN-LAST:event_jLabel1MouseExited

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ClosePane;
    private javax.swing.JLabel CurrentUserlbl;
    private javax.swing.JPanel ReceiverPanel;
    private javax.swing.JLabel ThisReceiiver;
    private javax.swing.JPanel UserList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel msgpane1;
    private javax.swing.JTextField txtmsg;
    // End of variables declaration//GEN-END:variables
}
